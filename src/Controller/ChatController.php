<?php

namespace App\Controller;


use App\Entity\Message;
use App\Form\MessageType;
use App\Service\Publish;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Rest\Route("/message", name="message")
 */
class ChatController extends AbstractFOSRestController
{

    /**
     * @Rest\Post("/create", name="notice_create")
     * @param Request $request
     * @param Publisher $publisher
     * @param SerializerInterface $serializer
     * @throws \Exception
     * @return JsonResponse
     */
    public function create(Request $request,Publisher $publisher,SerializerInterface $serializer): JsonResponse
    {
        $message=new Message();
        $form=$this->createForm(MessageType::class,$message);
        $request->request->replace([$form->getName() => $request->request->all()]);
        $form->handleRequest($request);
        if($form->isSubmitted()){

            $entityManager = $this->getDoctrine()->getManager();
            $message->setDateM(new \DateTime());
            $entityManager->persist($message);
            $entityManager->flush();
            $pub= new Publish();
            $msg=$entityManager->getRepository(Message::class)->getMsg();
            $pub($publisher,$serializer,$msg);
            return new JsonResponse(['msg' => 'success'],Response::HTTP_OK);
        }
        return new JsonResponse(['msg' =>  'check your inputs'], Response::HTTP_BAD_REQUEST);

    }

}

<?php


namespace App\Service;




use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\SerializerInterface;

class Publish
{
    public function __invoke(Publisher $publisher,SerializerInterface $serializer,$data)
    {

        $serializedData = $serializer->serialize($data,'json',
            [
                'circular_reference_limit' => 1,
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);
        $update = new Update(
            '',
            $serializedData
        );

        // The Publisher service is an invokable object
        $publisher($update);


    }
}